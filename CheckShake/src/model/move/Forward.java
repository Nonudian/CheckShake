package model.move;

import util.Direction;
import model.Tile;

public class Forward extends Move {
    public Forward(Direction direction, Tile aimedTile) {
        super(direction, aimedTile);
    }
}
