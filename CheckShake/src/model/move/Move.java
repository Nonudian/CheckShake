package model.move;

import util.Direction;
import model.Tile;

public abstract class Move {

    protected final Tile aimedTile;
    private final Direction direction;
    protected Move precedentMove;

    public Move(Direction direction, Tile aimedTile) {
        this.aimedTile = aimedTile;
        this.direction = direction;
        this.precedentMove = null;
    }
    
    public Direction getDirection() {
        return this.direction;
    }

    public Tile getAimedTile() {
        return this.aimedTile;
    }
    
    public void setPrecedentMove(Move precedentMove) {
        this.precedentMove = precedentMove;
    }
    
    public Move getPrecedentMove() {
        return this.precedentMove;
    }
}
