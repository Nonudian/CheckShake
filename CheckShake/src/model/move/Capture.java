package model.move;

import util.Direction;
import model.Piece;
import model.Tile;

public class Capture extends Move {
    private final Piece enemy;
    
    public Capture(Direction direction, Tile aimedTile, Piece enemy) {
        super(direction, aimedTile);
        this.enemy = enemy;
    }
    
    public Piece getEnemy() {
        return this.enemy;
    }
}
