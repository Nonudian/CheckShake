package model.brain;

import model.path.ForwardPath;
import model.path.CapturePath;
import java.util.ArrayList;
import java.util.Stack;
import javafx.geometry.Point2D;
import util.Direction;
import static util.Direction.*;
import controller.Game;
import model.Piece;
import model.Conductor;
import model.Tile;
import model.move.*;

public class Pawn extends Brain {

    public Pawn(Game game, Piece piece, Conductor conductor) {
        super(game, piece, conductor);
    }
    
    public Pawn(Brain brain) {
        super(brain);
    }
    
    @Override
    protected ArrayList<ForwardPath> getForwardPaths(Tile aimedTile, Direction direction) {
        ArrayList<ForwardPath> forwardPaths = new ArrayList();
        if (this.isFree(aimedTile)) {
            if (direction == NORTH_WEST || direction == NORTH_EAST) {
                ForwardPath toAdd = new ForwardPath(this.piece, new Forward(direction, aimedTile));
                toAdd.setThreats();
                forwardPaths.add(toAdd);
            }
        }
        return forwardPaths;
    }

    private ArrayList<CapturePath> getPathsFrom(Capture firstCapture) {
        ArrayList<CapturePath> capturePaths = new ArrayList();
        Stack<CapturePath> stack = new Stack();
        stack.push(new CapturePath(this.piece, firstCapture));

        while (!stack.isEmpty()) {
            CapturePath previousPath = stack.pop();
            Tile sourceTile = previousPath.getAimedTile();
            Direction notToCheck = previousPath.getLastDirection().getOpposed();
            ArrayList<Capture> nextCaptures = new ArrayList();
            
            for (Direction furtherDir : Direction.values()) {
                if (furtherDir != notToCheck) {
                    Point2D aimedCoords = this.getCoordsByDirection(sourceTile.getCoords(), furtherDir);
                    if (this.isReachable(aimedCoords)) {
                        Tile aimedTile = this.game.getTileByCoords(aimedCoords);
                        if (!this.isFree(aimedTile)) {
                            Piece obstaclePiece = aimedTile.getPiece();
                            if (this.isEnemy(obstaclePiece)) {
                                if (!previousPath.getEnemies().contains(obstaclePiece)) {
                                    Point2D furtherCoords = this.getCoordsByDirection(obstaclePiece.getCoords(), furtherDir);
                                    if (this.isReachable(furtherCoords)) {
                                        Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                                        if (isFree(furtherTile)) {
                                            nextCaptures.add(new Capture(furtherDir, furtherTile, obstaclePiece));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (nextCaptures.isEmpty()) {
                previousPath.setThreats();
                capturePaths.add(previousPath);
            } else {
                nextCaptures.forEach((newCapture) -> {
                    stack.push(new CapturePath(previousPath, newCapture));
                });
            }

        }
        return capturePaths;
    }

    @Override
    protected ArrayList<CapturePath> getCapturePaths(Tile aimedTile, Direction direction) {
        ArrayList<CapturePath> capturePaths = new ArrayList();
        if (!this.isFree(aimedTile)) {
            Piece obstaclePiece = aimedTile.getPiece();
            if (this.isEnemy(obstaclePiece)) {
                Point2D furtherCoords = this.getCoordsByDirection(obstaclePiece.getCoords(), direction);
                if (this.isReachable(furtherCoords)) {
                    Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                    if (isFree(furtherTile)) {
                        capturePaths.addAll(this.getPathsFrom(new Capture(direction, furtherTile, obstaclePiece)));
                    }
                }
            }
        }
        return capturePaths;
    }

    @Override
    public int getScore() {
        return 1;
    }
}
