package model.brain;

import model.request.ProtectionRequest;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import util.Direction;
import controller.Game;
import static java.lang.Integer.min;
import java.util.LinkedList;
import model.Piece;
import model.Conductor;
import util.Threat;
import model.Tile;
import model.path.*;
import model.request.Request;

public abstract class Brain {

    protected ArrayList<Path> paths;
    protected final Game game;
    protected final Piece piece;
    protected Conductor conductor;
    protected ArrayList<Threat> currentThreats;

    public Brain(Game game, Piece piece, Conductor conductor) {
        this.game = game;
        this.piece = piece;
        this.conductor = conductor;
        this.paths = new ArrayList();
    }

    public Brain(Brain brain) {
        this.game = brain.game;
        this.piece = brain.piece;
        this.conductor = brain.conductor;
        this.paths = new ArrayList();
    }

    public abstract int getScore();

    protected abstract ArrayList<ForwardPath> getForwardPaths(Tile aimedTile, Direction direction);

    protected abstract ArrayList<CapturePath> getCapturePaths(Tile aimedTile, Direction direction);

    protected Point2D getCoordsByDirection(Point2D coords, Direction direction) {
        return coords.add(direction.getUnitVector(this.piece.isInvertedAxis()));
    }

    protected boolean isReachable(Point2D coords) {
        return (coords.getX() > -1)
                && (coords.getX() < this.game.getDimension())
                && (coords.getY() > -1)
                && (coords.getY() < this.game.getDimension());
    }

    protected boolean isFree(Tile tile) {
        return tile.getPiece() == null;
    }

    protected boolean isEnemy(Piece obstaclePiece) {
        return obstaclePiece.getColor() != this.piece.getColor();
    }

    protected ArrayList<Path> getMovesToAdd(Point2D coords, Direction direction) {
        ArrayList<Path> movesToAdd = new ArrayList();
        if (this.isReachable(coords)) {
            // here add others move type checkings
            movesToAdd.addAll(this.getForwardPaths(this.game.getTileByCoords(coords), direction));
            movesToAdd.addAll(this.getCapturePaths(this.game.getTileByCoords(coords), direction));
        }
        return movesToAdd;
    }

    public ArrayList<Path> getPaths() {
        return this.paths;
    }

    public void analyseMoves() {
        // reset Paths
        this.paths.clear();
        this.currentThreats = this.getThreats(this.game.getTileByCoords(this.piece.getCoords()), null);

        // add available moves to path (check piece type, tiles, and direction)
        for (Direction direction : Direction.values()) {
            Point2D coords = this.getCoordsByDirection(this.piece.getCoords(), direction);
            this.paths.addAll(this.getMovesToAdd(coords, direction));
        }
    }

    public ArrayList<Threat> getCurrentThreats() {
        return this.currentThreats;
    }

    public boolean canMove() {
        return !this.paths.isEmpty();
    }

    public int getCrownedY() {
        if (this.piece.isInvertedAxis()) {
            return 0;
        }
        return this.game.getDimension() - 1;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public ArrayList<Brain> getClosestAllies() {
        ArrayList<Brain> closestAllies = new ArrayList();
        int alliesLimit = min(4, this.game.getTeamByColor(this.piece.getColor()).getPieces().size() - 1);
        LinkedList<Tile> tileQueue = new LinkedList();

        Point2D nextCoords = this.piece.getCoords();
        while (closestAllies.size() < alliesLimit) {
            for (Direction furtherDir : Direction.values()) {
                Point2D adjacentCoords = this.getCoordsByDirection(nextCoords, furtherDir);
                if (this.isReachable(adjacentCoords)) {
                    Tile adjacentTile = this.game.getTileByCoords(adjacentCoords);
                    if (!this.isFree(adjacentTile)) {
                        Piece obstaclePiece = adjacentTile.getPiece();
                        if (!this.isEnemy(obstaclePiece) && obstaclePiece.getBrain() != this) {
                            if (!closestAllies.contains(obstaclePiece.getBrain())) {
                                closestAllies.add(obstaclePiece.getBrain());
                            }
                        }
                    }
                    tileQueue.add(adjacentTile);
                }
            }
            nextCoords = tileQueue.pop().getCoords();
        }
        return closestAllies;
    }

    public ArrayList<Request> defineRequests(ArrayList<Brain> alliesFromCluster) {
        ArrayList<Request> requests = new ArrayList();
        alliesFromCluster.forEach((ally) -> {
            ally.getPaths().forEach((allyPath) -> {

                Tile allyAimedTile = allyPath.getAimedTile();

                // STATIC PROTECTIONS (BACK AND FRONT)
                if (!this.currentThreats.isEmpty()) {
                    this.currentThreats.forEach((threat) -> {
                        Direction dir = threat.getDirection();
                        if (allyAimedTile.getCoords().equals(this.getCoordsByDirection(this.piece.getCoords(), dir))
                                || (allyAimedTile.getCoords().equals(this.getCoordsByDirection(this.piece.getCoords(), dir.getOpposed())) && threat.getEnemy().isQueen())) {
                            requests.add(new ProtectionRequest(this, allyPath));
                        }
                    });
                }

                // [BOTH MOVING PROTECTION] (Only if tile is empty)
                for (int i = 0; i < this.paths.size(); i++) {
                    Path path = this.paths.get(i);
                    ArrayList<Threat> pathThreats = path.getThreats();
                    Tile pathAimedTile = path.getAimedTile();

                    if (!pathThreats.isEmpty()) {
                        pathThreats.forEach((threat) -> {
                            Direction dir = threat.getDirection();
                            if (!(allyAimedTile.getCoords().equals(pathAimedTile.getCoords()))) {
                                if (allyAimedTile.getCoords().equals(this.getCoordsByDirection(pathAimedTile.getCoords(), dir))
                                        || (allyAimedTile.getCoords().equals(this.getCoordsByDirection(pathAimedTile.getCoords(), dir.getOpposed())) && threat.getEnemy().isQueen())) {
                                    System.out.println("BoW: " + this.piece.getId() + " - " + pathAimedTile.getCoords() + " - " + allyPath.getOwnerId() + " - " + allyAimedTile.getCoords());
                                    requests.add(new ProtectionRequest(this, allyPath, path));
                                }
                            }
                        });
                    }
                }
            });
        });
        return requests;
    }

    public void voteRequests(ArrayList<Request> requests) {
        requests.forEach((unconcerning) -> {
            // play virtual move

            // [MATCHED PATH]
            Path matchedPath = unconcerning.getMatchedPath();
            Piece matchedPiece = null;
            Point2D matchedStartingCoords = null;
            Point2D matchedAimedCoords = null;
            
            if (matchedPath != null) {
                matchedPiece = matchedPath.getOwner();
                matchedStartingCoords = matchedPiece.getCoords();
                matchedAimedCoords = matchedPath.getAimedTile().getCoords();

                this.game.getTileByCoords(matchedStartingCoords).removePiece();
                this.game.getTileByCoords(matchedAimedCoords).setPiece(matchedPiece);

                if (matchedPath instanceof CapturePath) {
                    ((CapturePath) matchedPath).getEnemies().forEach((Piece enemy) -> {
                        this.game.getTileByCoords(enemy.getCoords()).removePiece();
                    });
                }
            }

            // [REQUESTED PATH]
            Path requestedPath = unconcerning.getRequestedPath();
            Piece requestedPiece = requestedPath.getOwner();
            Point2D requestedStartingCoords = requestedPiece.getCoords();
            Point2D requestedAimedCoords = requestedPath.getAimedTile().getCoords();

            this.game.getTileByCoords(requestedStartingCoords).removePiece();
            this.game.getTileByCoords(requestedAimedCoords).setPiece(requestedPiece);

            if (requestedPath instanceof CapturePath) {
                ((CapturePath) requestedPath).getEnemies().forEach((Piece enemy) -> {
                    this.game.getTileByCoords(enemy.getCoords()).removePiece();
                });
            }

            int currentThreatsScore = this.getThreats(this.game.getTileByCoords(this.piece.getCoords()), null).size();

            ArrayList<Path> projectedPaths = new ArrayList();
            for (Direction direction : Direction.values()) {
                Point2D coords = this.getCoordsByDirection(this.piece.getCoords(), direction);
                projectedPaths.addAll(this.getMovesToAdd(coords, direction));
            }

            if (!this.paths.isEmpty() && !projectedPaths.isEmpty()) {
                //POSSIBLE MOVES AT BEGINNING

                this.paths.forEach((oldPath) -> {
                    projectedPaths.forEach((newPath) -> { //UNBLOCKED MOVE(S)
                        if (oldPath.getScore() > newPath.getScore()
                                || currentThreatsScore > this.currentThreats.size()) {
                            if (matchedPath != null) {
                                matchedPath.decreaseVote();
                            }
                            requestedPath.decreaseVote();
                        } else if (oldPath.getScore() <= newPath.getScore()
                                || currentThreatsScore <= this.currentThreats.size()) {
                            if (matchedPath != null) {
                                matchedPath.increaseVote();
                            }
                            requestedPath.increaseVote();
                        }
                    });
                });
            } else if ((!this.paths.isEmpty() && projectedPaths.isEmpty())
                    || (this.paths.isEmpty() && !projectedPaths.isEmpty())) {
                //NO POSSIBLE MOVES AT BEGINNING / BLOCKED MOVES

                if (currentThreatsScore > this.currentThreats.size()) {
                    if (matchedPath != null) {
                        matchedPath.decreaseVote();
                    }
                    requestedPath.decreaseVote();
                } else if (currentThreatsScore <= this.currentThreats.size()) {
                    if (matchedPath != null) {
                        matchedPath.increaseVote();
                    }
                    requestedPath.increaseVote();
                }
            }

            // cancel virtual move
            // [REQUESTED PATH]
            this.game.getTileByCoords(requestedAimedCoords).removePiece();
            this.game.getTileByCoords(requestedStartingCoords).setPiece(requestedPiece);

            if (requestedPath instanceof CapturePath) {
                ((CapturePath) requestedPath).getEnemies().forEach((Piece enemy) -> {
                    this.game.getTileByCoords(enemy.getCoords()).setPiece(enemy);
                });
            }

            // [MATCHED PATH]
            if (matchedPath != null) {
                this.game.getTileByCoords(matchedAimedCoords).removePiece();
                this.game.getTileByCoords(matchedStartingCoords).setPiece(matchedPiece);

                if (matchedPath instanceof CapturePath) {
                    ((CapturePath) matchedPath).getEnemies().forEach((Piece enemy) -> {
                        this.game.getTileByCoords(enemy.getCoords()).setPiece(enemy);
                    });
                }
            }
        });
    }

    public ArrayList<Threat> getThreats(Tile aimedTile, Direction notToCheck) {
        ArrayList<Threat> threats = new ArrayList();
        // check each direction from aimedTile (but not the direction when coming from) 
        for (Direction furtherDir : Direction.values()) {
            Point2D furtherCoords = this.getCoordsByDirection(aimedTile.getCoords(), furtherDir);
            // if the further coord from aimedTile is reachable
            while (this.isReachable(furtherCoords)) {
                Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                // and if this tile possesses a piece
                if (!isFree(furtherTile)) {
                    // and if this is an enemy
                    Piece obstaclePiece = furtherTile.getPiece();
                    if (this.isEnemy(obstaclePiece)) {
                        Direction oppositeDirection = furtherDir.getOpposed();
                        if ((this.getCoordsByDirection(furtherCoords, oppositeDirection).equals(aimedTile.getCoords()))
                                || (obstaclePiece.getBrain() instanceof Queen)) {
                            // then we check the possibility to capture him
                            boolean found = false;
                            for (Threat threat : threats) {
                                if (threat.getEnemy().equals(obstaclePiece)) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                Point2D oppositeCoords = this.getCoordsByDirection(aimedTile.getCoords(), oppositeDirection);
                                if (this.isReachable(oppositeCoords)) {
                                    if (notToCheck == null || oppositeDirection != notToCheck) {
                                        if (isFree(this.game.getTileByCoords(oppositeCoords))) {
                                            threats.add(new Threat(furtherTile.getPiece(), oppositeDirection));
                                            break;
                                        }
                                        break;
                                    } else {
                                        threats.add(new Threat(furtherTile.getPiece(), oppositeDirection));
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    furtherCoords = getCoordsByDirection(furtherCoords, furtherDir);
                }
            }
        }
        return threats;
    }
}
