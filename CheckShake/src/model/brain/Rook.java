package model.brain;

import java.util.ArrayList;
import util.Direction;
import controller.Game;
import model.Piece;
import model.Conductor;
import model.Tile;
import model.path.CapturePath;
import model.path.ForwardPath;

public class Rook extends Brain {
    
    private final ArrayList<Piece> pieces;
    
    public Rook(Game game, Piece piece, Conductor conductor, ArrayList<Piece> pieces) {
        super(game, piece, conductor);
        this.pieces = pieces;
    }
    
    public Rook(Brain brain, ArrayList<Piece> pieces) {
        super(brain);
        this.pieces = pieces;
    }
    
    public int getHeight() {
        return pieces.size();
    }
    
    public void addPiece(Piece piece) {
        this.pieces.add(piece);
    }
    
    public void removePiece(Piece piece) {
        this.pieces.remove(piece);
    }
    
    @Override
    protected ArrayList<ForwardPath> getForwardPaths(Tile aimedTile, Direction direction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ArrayList<CapturePath> getCapturePaths(Tile aimedTile, Direction direction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public int getScore() {
        return this.getHeight();
    }
}
