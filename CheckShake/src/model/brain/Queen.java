package model.brain;

import model.path.ForwardPath;
import model.path.CapturePath;
import java.util.ArrayList;
import java.util.Stack;
import javafx.geometry.Point2D;
import util.Direction;
import controller.Game;
import model.Piece;
import model.Conductor;
import model.Tile;
import model.move.*;

public class Queen extends Brain {

    public Queen(Game game, Piece piece, Conductor conductor) {
        super(game, piece, conductor);
    }

    public Queen(Brain brain) {
        super(brain);
    }
    
    @Override
    protected ArrayList<ForwardPath> getForwardPaths(Tile aimedTile, Direction direction) {
        ArrayList<ForwardPath> forwardPaths = new ArrayList();
        if (this.isFree(aimedTile)) {
            ForwardPath previousPath = new ForwardPath(this.piece, new Forward(direction, aimedTile));
            previousPath.setThreats();
            forwardPaths.add(previousPath);

            boolean inLoop = true;
            while (inLoop) {
                Point2D furtherCoords = this.getCoordsByDirection(aimedTile.getCoords(), direction);
                if (this.isReachable(furtherCoords)) {
                    Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                    if (isFree(furtherTile)) {
                        ForwardPath toAdd = new ForwardPath(previousPath, new Forward(direction, furtherTile));
                        toAdd.setThreats();
                        forwardPaths.add(toAdd);
                        aimedTile = furtherTile;
                        previousPath = toAdd;
                    } else {
                        inLoop = false;
                    }
                } else {
                    inLoop = false;
                }
            }
        }
        return forwardPaths;
    }

    private ArrayList<CapturePath> getPathsFrom(Capture firstCapture) {
        ArrayList<CapturePath> capturePaths = new ArrayList();
        Stack<CapturePath> stack = new Stack();
        stack.push(new CapturePath(this.piece, firstCapture));

        while (!stack.isEmpty()) {
            CapturePath previousPath = stack.pop();
            Tile sourceTile = previousPath.getAimedTile();
            Direction notToCheck = previousPath.getLastDirection().getOpposed();
            ArrayList<Capture> nextCaptures = new ArrayList();

            for (Direction furtherDir : Direction.values()) {
                if (furtherDir != notToCheck) {
                    Point2D aimedCoords = this.getCoordsByDirection(sourceTile.getCoords(), furtherDir);
                    boolean inLoop = true;
                    while (this.isReachable(aimedCoords) && inLoop) {
                        Tile aimedTile = this.game.getTileByCoords(aimedCoords);
                        if (!this.isFree(aimedTile)) {
                            Piece obstaclePiece = aimedTile.getPiece();
                            if (this.isEnemy(obstaclePiece)) {
                                if (!previousPath.getEnemies().contains(obstaclePiece)) {
                                    Point2D furtherCoords = this.getCoordsByDirection(obstaclePiece.getCoords(), furtherDir);
                                    while (this.isReachable(furtherCoords)) {
                                        Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                                        if (isFree(furtherTile)) {
                                            nextCaptures.add(new Capture(furtherDir, furtherTile, obstaclePiece));
                                        } else {
                                            inLoop = false;
                                            break;
                                        }
                                        furtherCoords = getCoordsByDirection(furtherCoords, furtherDir);
                                    }
                                    aimedCoords = getCoordsByDirection(aimedCoords, furtherDir);
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            aimedCoords = getCoordsByDirection(aimedCoords, furtherDir);
                        }
                    }
                }
            }

            if (nextCaptures.isEmpty()) {
                previousPath.setThreats();
                capturePaths.add(previousPath);
            } else {
                nextCaptures.forEach((newCapture) -> {
                    stack.push(new CapturePath(previousPath, newCapture));
                });
            }

        }
        return capturePaths;
    }

    @Override
    protected ArrayList<CapturePath> getCapturePaths(Tile aimedTile, Direction direction) {
        ArrayList<CapturePath> capturePaths = new ArrayList();
        Point2D nextCoords = aimedTile.getCoords();

        boolean inLoop = true;
        while (isReachable(nextCoords) && inLoop) {
            Tile nextTile = this.game.getTileByCoords(nextCoords);
            if (!this.isFree(nextTile)) {
                Piece obstaclePiece = nextTile.getPiece();
                if (this.isEnemy(obstaclePiece)) {
                    Point2D furtherCoords = this.getCoordsByDirection(obstaclePiece.getCoords(), direction);
                    while (isReachable(furtherCoords)) {
                        Tile furtherTile = this.game.getTileByCoords(furtherCoords);
                        if (isFree(furtherTile)) {
                            capturePaths.addAll(this.getPathsFrom(new Capture(direction, furtherTile, obstaclePiece)));
                        } else {
                            //case 3: enemy tile is close to other filled tile (ally or enemy)
                            inLoop = false;
                            break;
                        }
                        furtherCoords = getCoordsByDirection(furtherCoords, direction);
                    }
                    nextCoords = getCoordsByDirection(nextCoords, direction);
                } else {
                    //case 2: tile has ally piece as content
                    break;
                }
            } else {
                //case 1: tile is empty
                nextCoords = getCoordsByDirection(nextCoords, direction);
            }
        }
        return capturePaths;
    }

    @Override
    public int getScore() {
        return 2;
    }
}
