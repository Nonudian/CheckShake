package model;

import java.util.ArrayList;
import model.brain.Brain;
import model.path.Path;
import model.request.Request;

public class Cluster {

    private final Brain owner;
    private final Conductor conductor;
    //private final ArrayList<Strategy> strategies;
    private final ArrayList<Request> requests;
    private final ArrayList<Path> paths;

    public Cluster(Conductor conductor, Brain owner) {
        this.conductor = conductor;
        this.owner = owner;
        //this.strategies = new ArrayList();
        this.requests = new ArrayList();
        this.paths = new ArrayList();
        this.buildPaths();
        this.buildRequests();
        this.askToVote();
    }

    public Conductor getConductor() {
        return this.conductor;
    }

    /*public ArrayList<Strategy> getStrategies() {
        return this.strategies;
    }*/

    public ArrayList<Request> getRequests() {
        return this.requests;
    }

    public ArrayList<Path> getAllPaths() {
        return this.paths;
    }

    private void buildPaths() {
        this.getBrains().forEach((brain) -> {
            this.paths.addAll(brain.getPaths());
        });
    }

    private void buildRequests() {
        this.getBrains().forEach((brain) -> {
            ArrayList<Brain> alliesFromCluster = new ArrayList(this.getBrains());
            alliesFromCluster.remove(brain);
            this.requests.addAll(brain.defineRequests(alliesFromCluster));
        });
    }

    private ArrayList<Request> getAlliesRequests(Brain brain) {
        ArrayList<Request> unconcerningRequests = new ArrayList();
        for (Request request : this.requests) {
            if (request.getAuthor() != brain) {
                unconcerningRequests.add(request);
            }
        }
        return unconcerningRequests;
    }

    private void askToVote() {
        this.getBrains().forEach((brain) -> {
            brain.voteRequests(this.getAlliesRequests(brain));
        });
    }

    public ArrayList<Brain> getBrains() {
        ArrayList<Brain> brainGroup = new ArrayList(this.owner.getClosestAllies());
        brainGroup.add(this.owner);
        return brainGroup;
    }

    public boolean isAlike(ArrayList<Brain> brainGroup) {
        return (this.getBrains().containsAll(brainGroup) && brainGroup.containsAll(this.getBrains()));
    }
}
