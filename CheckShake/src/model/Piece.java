package model;

import controller.Game;
import model.brain.Brain;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import model.brain.*;

public class Piece {

    private Brain brain;
    private Point2D coords;
    private final Team team;
    private final int id;
    private static int counterId = 0;

    public Piece(Game game, Team team) {
        this.brain = new Pawn(game, this, team.getConductor());
        this.team = team;
        this.id = counterId++;
    }

    public boolean isInvertedAxis() {
        return this.team.isInvertedAxis();
    }

    public Brain getBrain() {
        return this.brain;
    }
    
    public void setBrain(Brain brain) {
        this.brain = brain;
    }

    public Color getColor() {
        return this.team.getColor();
    }

    public Point2D getCoords() {
        return this.coords;
    }

    public void setCoords(Point2D coords) {
        this.coords = coords;
    }

    public void updateNextMove() {
        this.brain.analyseMoves();
    }

    public boolean canMove() {
        return this.brain.canMove();
    }
    
    public int getScore() {
        return this.brain.getScore();
    }
    
    public int getId() {
        return this.id;
    }
    
    public boolean isQueen() {
        return this.brain instanceof Queen;
    }
    
    public boolean isPawn() {
        return this.brain instanceof Pawn;
    }
    
    public boolean isRook() {
        return this.brain instanceof Rook;
    }
}
