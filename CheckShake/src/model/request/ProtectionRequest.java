package model.request;

import model.brain.Brain;
import model.path.Path;

public class ProtectionRequest extends Request {

    public ProtectionRequest(Brain author, Path requestedPath) {
        super(author, requestedPath);
    }

    public ProtectionRequest(Brain author, Path requestedPath, Path matchedPath) {
        super(author, requestedPath, matchedPath);
    }
}
