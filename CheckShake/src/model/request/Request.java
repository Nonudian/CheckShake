package model.request;

import model.brain.Brain;
import model.path.Path;

public abstract class Request {
    
    protected Brain author;
    protected Path requestedPath;
    protected Path matchedPath;
    
    public Request(Brain author, Path requestedPath) {
        this.author = author;
        this.requestedPath = requestedPath;
        this.matchedPath = null;
    }
            
    public Request(Brain author, Path requestedPath, Path matchedPath) {
        this.author = author;
        this.requestedPath = requestedPath;
        this.matchedPath = matchedPath;
    }
    
    public Brain getAuthor() {
        return this.author;
    }
    
    public Path getRequestedPath() {
        return this.requestedPath;
    }
    
    public Path getMatchedPath() {
        return this.matchedPath;
    }
    
    public boolean isAlike(Request request) {
        return (request.author.equals(this.author) &&
                request.requestedPath.equals(this.requestedPath) &&
                request.matchedPath.equals(this.matchedPath));
    }
}
