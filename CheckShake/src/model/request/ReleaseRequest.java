package model.request;

import model.brain.Brain;
import model.path.Path;

public class ReleaseRequest extends Request {

    public ReleaseRequest(Brain author, Path requestedPath, Path matchedPath) {
        super(author, requestedPath, matchedPath);
    }
}
