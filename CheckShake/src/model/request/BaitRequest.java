package model.request;

import model.Piece;
import model.brain.Brain;
import model.path.Path;

public class BaitRequest extends Request {

    private final Piece baited;
    
    public BaitRequest(Brain author, Path requestedPath, Path matchedPath, Piece baited) {
        super(author, requestedPath, matchedPath);
        this.baited = baited;
    }
    
    public Piece getBaited() {
        return this.baited;
    }
}
