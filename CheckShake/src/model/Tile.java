package model;

import javafx.geometry.Point2D;

public class Tile {

    private final Point2D coords;
    private Piece piece;

    public Tile(Point2D coords) {
        this.coords = coords;
        this.piece = null;
    }

    public Point2D getCoords() {
        return this.coords;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
        this.piece.setCoords(this.coords);
    }

    public void removePiece() {
        this.piece = null;
    }
}
