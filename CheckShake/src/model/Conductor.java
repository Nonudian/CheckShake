package model;

import model.path.Path;
import static java.lang.Integer.min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.brain.Brain;
import model.path.CapturePath;
import model.path.ForwardPath;
import util.PathComparator;

public class Conductor extends ArrayList<Path> {

    private final Team team;
    private final ArrayList<Cluster> clusters;
    private final int defaultMaxPiecesToMove = 3;

    public Conductor(Team team) {
        this.team = team;
        this.clusters = new ArrayList();
    }

    private boolean chooseCandidate(Path pathToAdd, ArrayList<Path> candidates) {
        Collections.sort(candidates, new PathComparator());
        return candidates.get(0) == pathToAdd;
    }

    public ArrayList<Path> conflictWith(Path pathToAdd) {
        ArrayList<Path> conflictWith = new ArrayList();
        for (int i = this.size() - 1; i >= 0; i--) {
            Path pathToCompare = this.get(i);
            // case 0 : owner  checking (all move types)
            if (pathToAdd.getOwner().equals(pathToCompare.getOwner())) {
                if (!conflictWith.contains(pathToCompare)) {
                    conflictWith.add(pathToCompare);
                }
            }

            // case 1 : same enemy checking (only capture move)
            if ((pathToAdd instanceof CapturePath) && (pathToCompare instanceof CapturePath)) {
                for (Piece enemy : ((CapturePath) pathToAdd).getEnemies()) {
                    for (Piece enemyToCompare : ((CapturePath) pathToCompare).getEnemies()) {
                        if (enemy.equals(enemyToCompare)) {
                            if (!conflictWith.contains(pathToCompare)) {
                                conflictWith.add(pathToCompare);
                            }
                        }
                    }
                }
            }

            // case 2 : crossing tile checking (all move types)
            for (Tile tileToCross : pathToAdd.getCrossingTiles()) {
                for (Tile tileToCompare : pathToCompare.getCrossingTiles()) {
                    if (tileToCross.equals(tileToCompare)) {
                        if (!conflictWith.contains(pathToCompare)) {
                            conflictWith.add(pathToCompare);
                        }
                    }
                }
            }
        }
        return conflictWith;
    }

    public boolean addToConductor(Path pathToAdd) {
        ArrayList<Path> conflicted = this.conflictWith(pathToAdd);
        boolean shouldAdd;
        if (conflicted.isEmpty()) {
            shouldAdd = true;
        } else {
            ArrayList<Path> candidates = new ArrayList(conflicted);
            candidates.add(pathToAdd);
            shouldAdd = this.chooseCandidate(pathToAdd, candidates);
            if (shouldAdd) {
                this.removeAll(conflicted);
            }
        }
        if (shouldAdd) {
            this.add(pathToAdd);
        }
        return shouldAdd;
    }

    public void updateConductor() {
        this.clear();
        this.team.getPieces().forEach(Piece::updateNextMove);
        this.createClusters();
        this.updatePaths();
    }
    
    private void createClusters() {
        this.clusters.clear();
        this.team.getPieces().forEach((piece) -> {
            this.createCluster(piece.getBrain());
        });
    }
    
    private void updatePaths() {
        this.team.getPieces().forEach((piece) -> {
            piece.getBrain().getPaths().forEach((path) -> {
                this.addToConductor(path);
            });
        });
    }
    
    private void createCluster(Brain brain) {
        ArrayList<Brain> brainGroup = brain.getClosestAllies();
        brainGroup.add(brain);
        
        boolean canAddCluster = true;
        for (Cluster cluster : this.clusters) {
            if(cluster.isAlike(brainGroup)) {
                canAddCluster = false;
                break;
            }
        }
        if (canAddCluster) {
            this.clusters.add(new Cluster(this, brain));
        }
    }

    public List<Path> getSelectedPaths() {
        Collections.sort(this, new PathComparator());
        int maxPiecesToMove = min(this.defaultMaxPiecesToMove, this.size());
        for (Path path : this.subList(0, maxPiecesToMove)) {
            if (maxPiecesToMove > 1 && path instanceof ForwardPath && path.getFinalScore() < 0) {
                maxPiecesToMove--;
            }
        }
        return this.subList(0, maxPiecesToMove);
    }
}
