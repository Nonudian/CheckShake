package model.path;

import model.Piece;
import model.move.Forward;
import model.move.Move;

public class ForwardPath extends Path {

    public ForwardPath(Piece owner, Forward firstStep) {
        super(owner);
        this.addStep(firstStep);
    }
    
    public ForwardPath(ForwardPath forwardSet, Forward forwardStep) {
        super(forwardSet);
        this.addStep(forwardStep);
    }
    
    @Override
    protected void addStep(Move move) {
        if(move instanceof Forward) {
            super.addStep(move);
        } else {
            throw new UnsupportedOperationException("ForwardSet can only contains Forward moves.");
        }
    }

    @Override
    public int getScore() {
        return this.getThreatsScore()+this.owner.getBrain().getCurrentThreats().size();
    }
}
