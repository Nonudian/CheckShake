package model.path;

import static java.lang.Math.abs;
import java.util.ArrayList;
import java.util.Collections;
import util.Direction;
import model.move.Move;
import model.Piece;
import util.Threat;
import model.Tile;

public abstract class Path {
    protected final Piece owner;
    protected Move lastMove;
    protected ArrayList<Threat> threats;
    protected int size;
    protected int voteResult;
    
    public Path(Piece owner) {
        this.owner = owner;
        this.lastMove = null;
        this.threats = new ArrayList();
        this.size = 0;
        this.voteResult = 0;
    }
    
    public Path(Path path) {
        this(path.getOwner());
        path.getSteps().forEach((step) -> {
            this.addStep(step);
        });
    }
    
    public Move getLastMove() {
        return this.lastMove;
    }
    
    public Direction getLastDirection() {
        return this.lastMove.getDirection();
    }
    
    public ArrayList<Tile> getCrossingTiles() {
        ArrayList<Tile> crossingTiles = new ArrayList();
        Move precedentMove = this.lastMove;
        while(precedentMove != null) {
            crossingTiles.add(precedentMove.getAimedTile());
            precedentMove = precedentMove.getPrecedentMove();
        }
        Collections.reverse(crossingTiles);
        return crossingTiles;
    }
    
    public Tile getAimedTile() {
        return this.lastMove.getAimedTile();
    }
    
    public int getDistanceToCrowned() {
        return abs((int) this.lastMove.getAimedTile().getCoords().getY() - this.owner.getBrain().getCrownedY());
    }
    
    public boolean isGreat() {
        return this.size > 1;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public ArrayList<Move> getSteps() {
        ArrayList<Move> moves = new ArrayList();
        Move precedentMove = this.lastMove;
        while(precedentMove != null) {
            moves.add(precedentMove);
            precedentMove = precedentMove.getPrecedentMove();
        }
        Collections.reverse(moves);
        return moves;
    }
    
    public Piece getOwner() {
        return this.owner;
    }
    
    public int getOwnerId() {
        return this.owner.getId();
    }
    
    public void setThreats() {
        this.threats = this.owner.getBrain().getThreats(
                this.lastMove.getAimedTile(), 
                this.lastMove.getDirection().getOpposed()
        );
    }
    
    public boolean isThreaten() {
        return this.threats.size() > 0;
    }
    
    public ArrayList<Threat> getThreats() {
        return this.threats;
    }
    
    public void increaseVote() {
        this.voteResult++;
    }
    
    public void decreaseVote() {
        this.voteResult--;
    }
    
    public int getVoteResult() {
        return this.voteResult;
    }
    
    public int getThreatsScore() {
        return -this.threats.size();
    }
    
    protected void addStep(Move move) {
        move.setPrecedentMove(this.lastMove);
        this.lastMove = move;
        this.size++;
    }

    public abstract int getScore();
    
    public int getFinalScore() {
        return this.getScore()+this.getVoteResult();
    }
}
