package model.path;

import java.util.ArrayList;
import java.util.Collections;
import model.Piece;
import model.move.Capture;
import model.move.Move;

public class CapturePath extends Path {
    
    public CapturePath(Piece owner, Capture firstStep) {
        super(owner);
        this.addStep(firstStep);
    }
    
    public CapturePath(CapturePath captureSet, Capture captureStep) {
        super(captureSet);
        this.addStep(captureStep);
    }
    
    public ArrayList<Piece> getEnemies() {
        ArrayList<Piece> enemies = new ArrayList();
        Move precedentMove = this.lastMove;
        while(precedentMove != null) {
            enemies.add(((Capture) precedentMove).getEnemy());
            precedentMove = precedentMove.getPrecedentMove();
        }
        Collections.reverse(enemies);
        return enemies;
    }
    
    public int getEnemiesScore() {
        int score = 0;
        Move precedentMove = this.lastMove;
        while(precedentMove != null) {
            score += ((Capture) precedentMove).getEnemy().getScore();
            precedentMove = precedentMove.getPrecedentMove();
        }
        return score;
    }
    
    @Override
    protected void addStep(Move move) {
        if(move instanceof Capture) {
            super.addStep(move);
        } else {
            throw new UnsupportedOperationException("CaptureSet can only contains Capture moves.");
        }
    }
    
    @Override
    public int getScore() {
        return this.getThreatsScore()+this.getEnemiesScore()+this.owner.getBrain().getCurrentThreats().size();
    }
}
