package model;

import controller.Game;
import java.util.ArrayList;
import javafx.scene.paint.Color;

public class Team {

    private final Color color;
    private final boolean invertedAxis;
    private final ArrayList<Piece> allPieces;
    private final Conductor conductor;
    private final Game game;

    public Team(Color color, boolean invertedAxis, Game game) {
        this.color = color;
        this.invertedAxis = invertedAxis;
        this.conductor = new Conductor(this);
        this.game = game;
        this.allPieces = new ArrayList();
    }

    public Color getColor() {
        return this.color;
    }
    
    public String getColorName() {
        if(!this.invertedAxis) {
            return "BLACK";
        } else {
            return "WHITE";
        }
    }

    public boolean isInvertedAxis() {
        return this.invertedAxis;
    }

    public Conductor getConductor() {
        return this.conductor;
    }
    
    public ArrayList<Piece> getPieces() {
        return this.allPieces;
    }
    
    public void initPieces(int piecesPerTeam) {
        for(int i = 0; i < piecesPerTeam; i++) {
            this.allPieces.add(new Piece(this.game, this));
        }
    }
    
    public void removePiece(Piece piece) {
        this.allPieces.remove(piece);
    }
}
