
package model;

import javafx.geometry.Point2D;

public class Board {
    
    private Tile[][] tiles;
    private final int dimension;
    
    public Board(int dimension) {
        this.dimension = dimension;
        this.initTiles();
    }
    
    public int getDimension() {
        return this.dimension;
    }
    
    public Tile[][] getTiles() {
        return this.tiles;
    }
    
    public Tile getTile(int x, int y) {
        return this.tiles[x][y];
    }
    
    private void initTiles() {
        this.tiles = new Tile[this.dimension][this.dimension];
        for (int y = 0; y < this.dimension; y++) {
            for (int x = 0; x < this.dimension; x++) {
                this.tiles[x][y] = new Tile(new Point2D(x, y));
            }
        }
    }
    
    public Tile getTileByCoords(Point2D coords) {
        return this.getTile((int) coords.getX(), (int) coords.getY());
    }
}
