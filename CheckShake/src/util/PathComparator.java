package util;

import model.path.ForwardPath;
import model.path.Path;
import model.path.CapturePath;
import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;
import model.brain.Pawn;

public class PathComparator implements Comparator<Path> {
    
    @Override
    public int compare(Path ms1, Path ms2) {
        if (ms1 instanceof CapturePath) {
            if (ms2 instanceof ForwardPath) {
                return -1;
            }
            return this.compareCaptureSize((CapturePath) ms1, (CapturePath) ms2);
        } else {
            if (ms2 instanceof CapturePath) {
                return 1;
            }
            return this.compareFinalScore(ms1, ms2);
        }
    }

    public int compareCaptureSize(CapturePath cs1, CapturePath cs2) {
        if (cs1.getSize() > cs2.getSize()) {
            return -1;
        } else if (cs1.getSize() < cs2.getSize()) {
            return 1;
        }
        return this.compareFinalScore(cs1, cs2);
    }
    
    public int compareFinalScore(Path ms1, Path ms2) {
        int score = ms2.getFinalScore() - ms1.getFinalScore();
        if (score > 0) {
            return 1;
        } else if (score < 0) {
            return -1;
        }
        return this.compareScore(ms1, ms2);
    }

    public int compareScore(Path ms1, Path ms2) {
        int score = ms2.getScore() - ms1.getScore();
        if (score > 0) {
            return 1;
        } else if (score < 0) {
            return -1;
        }
        return this.compareQueen(ms1, ms2);
    }

    public int compareQueen(Path ms1, Path ms2) {
        if (ms1.getOwner().getBrain() instanceof Pawn && 
            ms2.getOwner().getBrain() instanceof Pawn) {
            return this.compareDistance(ms1, ms2);
        }
        return this.random();
    }

    public int compareDistance(Path ms1, Path ms2) {
        int diff = ms2.getDistanceToCrowned() - ms1.getDistanceToCrowned();
        if (diff > 0) {
            return -1;
        } else if (diff < 0) {
            return 1;
        }
        return this.random();
    }
    
    public int random() {
        return ThreadLocalRandom.current().nextInt(-1, 2);
    }
}
