package util;

import model.Piece;
import util.Direction;

public class Threat {
    private final Piece enemy;
    private final Direction direction;
    
    public Threat(Piece enemy, Direction direction) {
        this.enemy = enemy;
        this.direction = direction;
    }
    
    public Piece getEnemy() {
        return this.enemy;
    }
    
    public Direction getDirection() {
        return this.direction;
    }
}
