package util;

import javafx.geometry.Point2D;

public enum Direction {
    NORTH_WEST, NORTH_EAST, SOUTH_WEST, SOUTH_EAST;
    
    public Point2D getUnitVector(boolean isInverted) {
        int xDir = 0, yDir = 0;
        switch (this) {
            case NORTH_WEST:
                xDir = -1;
                yDir = 1;
                break;
            case NORTH_EAST:
                xDir = 1;
                yDir = 1;
                break;
            case SOUTH_WEST:
                xDir = -1;
                yDir = -1;
                break;
            case SOUTH_EAST:
                xDir = 1;
                yDir = -1;
                break;
        }
        if (isInverted) {
            xDir = -xDir;
            yDir = -yDir;
        }
        return new Point2D(xDir, yDir);
    }
    
    public Direction getOpposed() {
        switch(this) {
            case NORTH_WEST:
                return SOUTH_EAST;
            case NORTH_EAST:
                return SOUTH_WEST;
            case SOUTH_WEST:
                return NORTH_EAST;
            case SOUTH_EAST:
                return NORTH_WEST;
            default:
                return null;
        }
    };
}
