package checkshake;

import java.net.URL;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import controller.Game;

import view.GameView;

public class CheckShake extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage menu) {

        menu.setTitle("Welcome to CheckShake");
        menu.getIcons().add(new Image(getClass().getResourceAsStream("assets/images/checkicon.jpg")));

        Button aboutBtn = new Button();
        Button gameBtn = new Button();
        aboutBtn.setMinSize(200, 75);
        gameBtn.setMinSize(200, 75);
        aboutBtn.setText("About this project");
        gameBtn.setText("Start a game");

        aboutBtn.setOnAction((ActionEvent event) -> {
            //Implement here the about button
        });
        gameBtn.setOnAction((ActionEvent event) -> {
            Game game = new Game();
            GameView gameview = new GameView(game);
            gameview.setTitle("CheckShake Game (RUNNING) - Press SPACE to PAUSE");
            gameview.getIcons().add(new Image(CheckShake.this.getClass().getResourceAsStream("assets/images/checkicon.jpg")));
            menu.close();
        });

        GridPane menuGrid = new GridPane();
        menuGrid.setId("menuGrid");
        menuGrid.setAlignment(Pos.CENTER);
        menuGrid.setPadding(new Insets(10));
        menuGrid.setVgap(8);

        menuGrid.add(aboutBtn, 0, 0);
        menuGrid.add(gameBtn, 0, 1);
        menuGrid.setMinSize(500, 400);
        Scene menuScene = new Scene(menuGrid);
        URL url = this.getClass().getResource("assets/style.css");
        if (url == null) {
            System.out.println("Resource not found. Aborting.");
            System.exit(-1);
        }
        String css = url.toExternalForm();
        menuScene.getStylesheets().add(css);

        menu.setScene(menuScene);
        menu.sizeToScene();
        menu.show();
    }
}
