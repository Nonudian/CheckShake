package view;

import model.path.ForwardPath;
import model.path.Path;
import model.path.CapturePath;
import java.util.List;
import javafx.animation.TranslateTransition;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import static javafx.scene.shape.StrokeType.OUTSIDE;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import controller.Game;
import model.Piece;
import model.Team;
import model.Tile;
import model.brain.Queen;
import model.move.*;

public class GameView extends Stage {

    private final BorderPane root;
    private final HBox greatRoot;
    private final TilePane grid;
    private final TilePane gridTile;
    private final StackPane gridsHolder;
    private Scene scene;
    private final Game game;
    private boolean keepPausing;
    private final FlowPane steps;
    private final ScrollPane scroll;

    public GameView(Game game) {
        this.game = game;
        this.greatRoot = new HBox();
        this.root = new BorderPane();
        this.grid = new TilePane();
        this.gridTile = new TilePane();
        this.gridsHolder = new StackPane(this.gridTile, this.grid);
        this.keepPausing = false;
        this.steps = new FlowPane();
        this.scroll = new ScrollPane();

        this.drawGame(this.game.getTiles());
        this.display();
        this.gameLaunch();
    }

    private void addStep(String step) {
        Label stepLabel = new Label(step);
        stepLabel.setFont(new Font("Trebuchet MS", 12));
        stepLabel.setTextFill(Color.WHITE);
        this.steps.getChildren().add(0, stepLabel);
    }

    private void applyMove(Path path, int index) {
        Move move = path.getSteps().get(index);
        int oldX, oldY;
        int duration;
        if(path instanceof ForwardPath) {
            duration = this.game.getDelayBeforeUpdate();
        } else {
            duration = this.game.getDelayBeforeUpdate() / path.getSteps().size();
        }
        if (index > 0 && !(path instanceof ForwardPath)) {
            Tile oldTile = path.getSteps().get(index - 1).getAimedTile();
            oldX = (int) oldTile.getCoords().getX();
            oldY = (int) oldTile.getCoords().getY();
        } else {
            Piece owner = path.getOwner();
            oldX = (int) owner.getCoords().getX();
            oldY = (int) owner.getCoords().getY();
        }

        int newX = (int) move.getAimedTile().getCoords().getX();
        int newY = (int) move.getAimedTile().getCoords().getY();
        int oldInd = (int) oldY * this.game.getDimension() + (int) oldX;
        int newInd = (int) newY * this.game.getDimension() + (int) newX;

        StackPane oldStack = (StackPane) this.grid.getChildren().get(oldInd);

        // circlePiece and ids
        Circle circle = (Circle) oldStack.getChildren().get(1);
        circle.setStroke(Color.RED);

        TranslateTransition tt = new TranslateTransition(Duration.millis(duration), oldStack);
        tt.setByX(50 * (newX - oldX));
        tt.setByY(50 * (newY - oldY));
        tt.setOnFinished(e -> {
            StackPane newStack = (StackPane) this.grid.getChildren().get(newInd);
            newStack.getChildren().addAll(oldStack.getChildren().remove(1), oldStack.getChildren().remove(1));
            oldStack.setTranslateX(0);
            oldStack.setTranslateY(0);

            if (move instanceof Capture) {
                int enemyInd = (int) ((Capture) move).getEnemy().getCoords().getY() * this.game.getDimension() + (int) ((Capture) move).getEnemy().getCoords().getX();
                StackPane enemyStack = (StackPane) this.grid.getChildren().get(enemyInd);
                enemyStack.getChildren().removeAll(enemyStack.getChildren().get(1), enemyStack.getChildren().get(2));
            }

            if (path.getLastMove() != move) {
                this.applyMove(path, index + 1);
            } else {
                if(path.getOwner().getBrain() instanceof Queen) {
                    this.setPieceStroke(circle, Color.GOLD);
                    circle.setStrokeType(OUTSIDE);
                    circle.getStrokeDashArray().addAll(3.0,7.0,3.0,7.0);
                } else {
                    this.setPieceStroke(circle, path.getOwner().getColor().invert());
                }
                if (!this.keepPausing) {
                    this.game.play();
                }
            }
        });
        tt.play();
    }
    
    private void setPieceStroke(Circle piece, Color color) {
        piece.setStroke(color);
    }

    private void gameLaunch() {
        this.game.setGameView(this);
        this.game.play();
    }

    public void update(List<Path> paths, int counterPieces, int canMovePieces, boolean shouldEndQuickly) {
        String step = this.logTurn(counterPieces, canMovePieces, shouldEndQuickly) + "\n";
        int i = 0;
        while (i < paths.size()) {
            if (!this.keepPausing) {
                Path currPath = paths.get(i);
                step += this.logPath(currPath);
                if(currPath instanceof ForwardPath) {
                    this.applyMove(currPath, currPath.getSize()-1);
                } else {
                    this.applyMove(currPath, 0);
                }
                i++;
            }
        }
        this.addStep(step + "\n");
    }

    private String logPath(Path path) {
        Point2D oldCoords = path.getOwner().getCoords();
        Point2D newCoords = path.getAimedTile().getCoords();
        String name = path.getClass().getSimpleName();
        String str = "[MOVE: ";
        if (path instanceof CapturePath && ((CapturePath) path).isGreat()) {
            str += "Great ";
        }
        if (path.getOwner().getBrain() instanceof Queen) {
            str += "Queen ";
        }
        str += name.substring(0, name.length() - 4) + "] - vote: "
                + path.getVoteResult() + " - score: "
                + path.getScore() + " - ("
                + oldCoords.getX()
                + ", "
                + oldCoords.getY()
                + ")"
                + " -> ("
                + newCoords.getX()
                + ", "
                + newCoords.getY()
                + ")\n";
        return str;
    }

    private String logTurn(int counterPieces, int canMovePieces, boolean shouldEndQuickly) {
        String str = "[TURN " + this.game.getTurn() + "] - " + this.game.getNextTeam().getColorName()
                + " - "
                + counterPieces
                + " pieces remaining"
                + " - "
                + canMovePieces
                + " pieces can move";
        if(shouldEndQuickly) {
            str += "\n[ONLY QUEENS REMAINING] - Game would end before TURN "+(this.game.getTurn()+this.game.getFinalCountdown());
        }
        return str;
    }

    public void endView() {
        String str = "[END AT TURN " + this.game.getTurn() + "]\n";
        for (Team team : this.game.getTeams()) {
            str += "[" + team.getColorName() + "] - " + team.getPieces().size() + " pieces remaining\n";
        }
        this.addStep(str + "\n");
    }

    private void drawGame(Tile[][] tiles) {
        GridPane borderX = new GridPane();
        TilePane borderY = new TilePane();

        Rectangle rc = new Rectangle(30, 30);
        rc.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/checkshake/assets/images/checkerboard.jpg"))));
        borderX.add(rc, 0, 0);

        for (int x = 0; x < this.game.getDimension(); x++) {
            Rectangle rx = new Rectangle(50, 30);
            rx.setFill(Color.MAROON);
            Text coordX = new Text(25, 25, "");
            coordX.setFill(Color.WHITE);
            coordX.setTextAlignment(TextAlignment.CENTER);
            coordX.setText(String.valueOf(x));
            borderX.add(new StackPane(rx, coordX), x + 1, 0);
        }

        for (int y = 0; y < this.game.getDimension(); y++) {
            Rectangle ry = new Rectangle(30, 50);
            ry.setFill(Color.MAROON);
            Text coordY = new Text(25, 25, "");
            coordY.setFill(Color.WHITE);
            coordY.setTextAlignment(TextAlignment.CENTER);
            coordY.setText(String.valueOf(y));
            borderY.getChildren().add(new StackPane(ry, coordY));
            for (int x = 0; x < this.game.getDimension(); x++) {
                Rectangle squareTile = new Rectangle(50, 50);
                StackPane myStack = new StackPane();

                if ((x + y) % 2 == 1) {
                    squareTile.setFill(Color.PERU);
                    Rectangle rectTransp = new Rectangle(50, 50, Color.TRANSPARENT);
                    Piece piece = tiles[x][y].getPiece();
                    if (piece != null) {
                        Circle circlePiece = new Circle(25, 25, 20, Color.TRANSPARENT);
                        Text ids = new Text(25, 25, "");

                        circlePiece.setFill(piece.getColor());
                        this.setPieceStroke(circlePiece, piece.getColor().invert());
                        circlePiece.setStrokeWidth(2);
                        ids.setText(String.valueOf(piece.getId()));
                        ids.setFill(piece.getColor().invert());
                        ids.setTextAlignment(TextAlignment.CENTER);

                        myStack.getChildren().addAll(rectTransp, circlePiece, ids);
                    } else {
                        myStack.getChildren().add(rectTransp);
                    }
                } else {
                    squareTile.setFill(Color.BURLYWOOD);
                }
                this.gridTile.getChildren().add(new StackPane(squareTile));
                this.grid.getChildren().add(myStack);
            }
        }

        this.steps.setPadding(new Insets(10));
        this.scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.scroll.setContent(this.steps);
        this.scroll.setPannable(true);
        this.scroll.setFitToHeight(true);
        this.scroll.setFitToWidth(true);
        this.scroll.setId("scrollLog");
        this.scroll.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.SPACE) {
                event.consume();
            }
        });
        this.addStep("Waiting for game launch...");
        
        this.gridsHolder.setMaxSize(this.game.getDimension() * 50, this.game.getDimension() * 50);
        borderX.setMaxSize(this.game.getDimension() * 50, 30);
        borderY.setMaxSize(30, this.game.getDimension() * 50);

        this.root.setTop(borderX);
        this.root.setLeft(borderY);
        this.root.setCenter(this.gridsHolder);
        this.greatRoot.getChildren().add(this.scroll);
        this.greatRoot.getChildren().add(this.root);
    }

    private void display() {
        this.scene = new Scene(this.greatRoot);
        this.scene.getStylesheets().add("/checkshake/assets/style.css");

        this.scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.SPACE) {
                if (this.keepPausing) {
                    this.keepPausing = false;
                    this.game.play();
                    this.addStep("----------------------------- GAME ON ------------------------------\n");
                    this.setTitle("CheckShake Game (RUNNING) - Press SPACE to PAUSE");
                } else {
                    this.keepPausing = true;
                    this.game.pause();
                    this.addStep("----------------------------- GAME OFF -----------------------------\n");
                    this.setTitle("CheckShake Game (PAUSED) - Press SPACE to UNPAUSE");
                }
            }
        });

        this.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, event -> {
            this.close();
        });

        this.setScene(this.scene);
        this.setResizable(false);
        this.sizeToScene();
        this.centerOnScreen();

        this.show();
    }
}
