package controller;

import model.path.CapturePath;
import model.path.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javafx.animation.Animation;
import static javafx.animation.Animation.Status.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.*;
import javafx.util.Duration;
import model.Board;
import model.Piece;
import model.Conductor;
import model.Team;
import model.Tile;
import model.brain.Queen;
import view.GameView;

public class Game {

    private final Timeline timeline;
    private Team[] teams;
    private int nextTeam;
    private int defaultTeam;
    private int turn;
    private GameView gameview;
    private final int delayBeforeUpdate;
    private int finalCountdown;
    private final Board board;

    public Game() {
        this.turn = 1;
        this.gameview = null;
        this.delayBeforeUpdate = 1000;
        this.finalCountdown = 10;
        this.timeline = new Timeline(new KeyFrame(Duration.millis(delayBeforeUpdate), event -> update()));
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.board = new Board(10);
        this.initTeams();
        this.initPieces();
    }
    
    public int getDimension() {
        return this.board.getDimension();
    }

    public void setGameView(GameView gameview) {
        this.gameview = gameview;
    }

    public int getDelayBeforeUpdate() {
        return this.delayBeforeUpdate;
    }

    public int getFinalCountdown() {
        return this.finalCountdown;
    }

    public Timeline getTimeline() {
        return this.timeline;
    }

    private void initTeams() {
        this.teams = new Team[2];
        this.teams[0] = new Team(BLACK, false, this);
        this.teams[1] = new Team(WHITE, true, this);
        int team = ThreadLocalRandom.current().nextInt(0, this.teams.length);
        this.nextTeam = team;
        this.defaultTeam = team;
    }

    private void initPieces() {
        int dimension = this.getDimension();
        int width = dimension / 2;
        int height = (dimension - 2) / 2;
        int piecesPerTeam = width * height;
        if (dimension % 2 == 1) {
            piecesPerTeam += (height / 2);
        }

        for (Team team : this.teams) {
            team.initPieces(piecesPerTeam);
        }
        this.standPieces(piecesPerTeam);
    }

    private void standPieces(int piecesPerTeam) {
        ArrayList<Piece> pieces = getPieces();
        int dimension = this.getDimension();
        int i = 0;
        for (int y = 0; y < (dimension / 2) - 1; y++) { // Black
            for (int x = 0; x < dimension; x++) {
                if ((x + y) % 2 == 1) {
                    this.board.getTile(x, y).setPiece(pieces.get(i));
                    i++;
                }
            }
        }
        for (int y = dimension - 1; dimension / 2 < y; y--) { // White
            for (int x = 0; x < dimension; x++) {
                if ((x + y) % 2 == 1) {
                    if (i == 2 * piecesPerTeam) {
                        break;
                    }
                    this.board.getTile(x, y).setPiece(pieces.get(i));
                    i++;
                }
            }
        }
    }
    
    public Board getBoard() {
        return this.board;
    }

    public Tile[][] getTiles() {
        return this.board.getTiles();
    }

    public Tile getTileByCoords(Point2D coords) {
        return this.board.getTileByCoords(coords);
    }

    private ArrayList<Piece> getPieces() {
        ArrayList<Piece> pieces = new ArrayList();
        for (Team team : this.teams) {
            pieces.addAll(team.getPieces());
        }
        return pieces;
    }

    private boolean shouldEndQuickly() {
        for (Piece piece : this.getPieces()) {
            if (!(piece.getBrain() instanceof Queen)) {
                return false;
            }
        }
        return true;
    }

    public void movePiece(Path path) {
        Piece piece = path.getOwner();
        Tile aimedTile = path.getAimedTile();

        this.getTileByCoords(piece.getCoords()).removePiece(); // old Tile
        aimedTile.setPiece(piece); // new Tile

        if (!(piece.getBrain() instanceof Queen)
                && (aimedTile.getCoords().getY() == 0 && piece.isInvertedAxis())
                || (aimedTile.getCoords().getY() == (this.getDimension() - 1) && !piece.isInvertedAxis())) {
            piece.setBrain(new Queen(piece.getBrain()));
        }

        if (path instanceof CapturePath) {
            ((CapturePath) path).getEnemies().forEach((Piece enemy) -> {
                this.getTileByCoords(enemy.getCoords()).removePiece();
                Team team = this.getTeamByColor(enemy.getColor());
                team.removePiece(enemy);
            });
        }
    }

    public Team[] getTeams() {
        return this.teams;
    }

    public Team getTeamByColor(Color color) {
        if (color == BLACK) {
            return this.teams[0];
        } else {
            return this.teams[1];
        }
    }

    public Team getNextTeam() {
        return this.teams[this.nextTeam];
    }

    private void switchNextTeam() {
        this.nextTeam = 1 - this.nextTeam;
        if (this.nextTeam == this.defaultTeam) {
            this.turn++;
            if (this.shouldEndQuickly()) {
                this.finalCountdown--;
            }
        }
    }

    public int getTurn() {
        return this.turn;
    }

    public void update() {
        if (this.timeline.getStatus() == RUNNING) {
            Conductor conductor = this.getNextTeam().getConductor();
            conductor.updateConductor();
            if (conductor.isEmpty() || this.finalCountdown < 1) {
                this.stop();
            } else {
                this.pause();
                List<Path> paths = conductor.getSelectedPaths();
                this.gameview.update(paths, this.getNextTeam().getPieces().size(), conductor.size(), this.shouldEndQuickly());
                paths.forEach((path) -> {
                    this.movePiece(path);
                });
                this.switchNextTeam();
            }
        }
    }

    public void play() {
        this.timeline.play();
    }

    public void pause() {
        this.timeline.pause();
    }

    public void stop() {
        this.timeline.stop();
        this.gameview.endView();
    }
}
