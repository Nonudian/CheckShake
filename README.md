# CheckShake (v0.4.2)

***fr:***  
L’objectif du projet est de proposer une modélisation distribuée d’un jeu de dames, dans lequel chaque pièce réfléchit individuellement puis collectivement afin d'établir des stratégies avec son équipe, dans le but de vaincre l’équipe adverse.  
Développée par l'équipe ElChaBo, composée de BOFFY Thomas, CHAZOT William et EL KHADIR Najib.  

***en:***  
Our project was designed to simulate a multi-agent system using a distributed modelling, through a Checkers game. Each game piece reflects individually, and then collectively with its team, in order to introduce interesting strategies which would allow to beat the opponent.  
Application developped by the ElChaBo team, composed by BOFFY Thomas, CHAZOT William and EL KHADIR Najib.  

Demo : https://www.youtube.com/watch?v=R-ubsgGQgo8

---
### ***Configuration and functionning only described in french*** 
# Prérequis
  1. Installer JDK version 1.8
  2. Installer NetBeans version 8.2
  3. S'assurer que JavaFX est inclus dans le JDK

# Lancer l'application (Veuillez respecter les prérequis avant de passer à cette étape)
  1. Télécharger l'archive zip de l'application depuis le depôt GitHub
  2. Extraire le dossier "CheckShake-master"
  3. Sur votre IDE NetBeans, cliquez sur "New Project"
  4. Sélectionner "JavaFX" puis "JavaFX Project from Existing Sources"
  5. Appuyer sur Next, donner un nom à votre projet
  6. Appuyer sur Next
  7. Dans la section "Source Package Folders" appuyer sur "Add Folder..."
  8. Ajouter le dossier "CheckShake-master/CheckShake/src"
  9. Appuyer sur Next, puis Finish
  10. Vous pouvez désormais consulter & éditer le code source
  > Pour lancer l'application rendez-vous dans le package "checkshake" et exécuter la classe "CheckShake.java"

---
# Fonctionnement 
  1. À chaque tour, le chef d'orchestre de l'équipe (Conductor) demande à chaque pièce (Piece/Brain) quels sont ses voisins, afin de constituer les clusters (Cluster).
  2. Chaque pièce détermine ses coups possibles et établit des requêtes de stratégies égoîstes en se basant sur ses voisins.
  3. Chaque cluster récolte les requêtes (Request) de ses pièces.
  4. Chaque cluster demande à ses pièces quels sont les coups (Path) qui les arrangent afin de procéder au vote sur les coups.
  5. Le chef d'ochestre récupère alors les meilleurs coups possibles et les transmet au jeu (Game).

# Prochaine étape
  - Chaque cluster devra construire des stratégies collectives pour son groupe de pièces.
  - Un cluster pourra alors renvoyer ses stratégies collectives votées et choisies localement, au chef d'orchestre.
---
